package com.warmupApp;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class DisplayResult extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_result);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.display_result, menu);
		Intent intent = getIntent();
		String count = intent.getStringExtra(MainActivity.EXTRA_COUNT);
		String user = intent.getStringExtra(MainActivity.EXTRA_USER);
		((TextView) findViewById(R.id.textView1)).setText("Welcome "+user+"!\n"+"You have logged in "+count+" times.");
		return true;
	}
	
	public void goback(View view) {
		new SessionStorage(this).deleteAll();
    	finish();
    }

}
