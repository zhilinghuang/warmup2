package com.warmupApp;

import android.R.string;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SessionStorage extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 2;
    public static final String TABLE_NAME = "userandcount";
    public static final String userName = "Username";
    public static final String count = "Count";
    private static final String TABLE_CREATE =
                "CREATE TABLE " + TABLE_NAME + " ("+userName+" TEXT, "+count+" TEXT);";
    SessionStorage(Context context) {
        super(context, "SessionStorage", null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_CREATE);
    }

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}
	
	public void addUserCount(String user, String count) {
		SQLiteDatabase db = this.getWritableDatabase();
    	ContentValues values = new ContentValues();
    	values.put(SessionStorage.userName, user);
    	values.put(SessionStorage.count, count);
        db.insert(SessionStorage.TABLE_NAME, null, values);
	}
	
	public void deleteAll() {
		this.getWritableDatabase().delete(TABLE_NAME, null, null);
	}
	
	
}