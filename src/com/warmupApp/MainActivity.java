package com.warmupApp;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnHoverListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView.OnEditorActionListener;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.widget.TextView;
import android.database.*;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
public class MainActivity extends Activity {
	public final static String EXTRA_COUNT = "com.warmupApp.result";
	public final static String EXTRA_USER = "com.warmupApp.user";
	public final static String addUserURL = "http://hidden-brook-4935.herokuapp.com/users/add";
	public final static String loginURL = "http://hidden-brook-4935.herokuapp.com/users/login";
	
	private final TextWatcher  textWatcher = new TextWatcher() {    
         public void beforeTextChanged(CharSequence s, int start, int count, int after){}
         public void onTextChanged(CharSequence s, int start, int before, int count){}
         public void afterTextChanged(Editable s)
         { 
        	 checkUserPassword();	
         }
	 };
	 
	 private final OnFocusChangeListener textFocus = new OnFocusChangeListener() {          
	        public void onFocusChange(View v, boolean hasFocus) {
	            if(!hasFocus)
	                checkUserPassword();
	        }
	    };
	 
	 
	public boolean checkUserPassword() {
		String result = "";
		boolean correct = true;
		 if (((EditText) findViewById(R.id.editText1)).length()==0) {
			result+= "Username cannot be empty;\n" ;
			correct = false;
		 } else if (((EditText) findViewById(R.id.editText1)).length()>128) {
			 result+= "Username cannot be longer than 128;\n" ;
			 correct = false;
		 } 
		 if (((EditText) findViewById(R.id.editText2)).length()>128) {
				result+= "Password cannot be longer than 128;\n";
				correct = false;
		 }
		 setMessage(result);
		 return correct;
	}
	 
	
	protected void setCheckEditText() {
		//((EditText) findViewById(R.id.editText1)).addTextChangedListener(textWatcher);
		//((EditText) findViewById(R.id.editText2)).addTextChangedListener(textWatcher);
		((EditText) findViewById(R.id.editText1)).setOnFocusChangeListener(textFocus);
		((EditText) findViewById(R.id.editText2)).setOnFocusChangeListener(textFocus);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		setCheckEditText();
		SQLiteDatabase db = new SessionStorage(this).getWritableDatabase();
    	Cursor log = db.query(SessionStorage.TABLE_NAME, null, null, null,null, null, null);
    	if (log.getCount()>=1) {
    		log.moveToFirst();
    		String userName = log.getString(log.getColumnIndex(SessionStorage.userName));
    		String count = log.getString(log.getColumnIndex(SessionStorage.count));
    		this.jumpToDisplay(userName, count );
    	}
    	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
    
	
	
    public void addUser(View view) {
    	try {
    		if (checkUserPassword()) {
		    	String user = ((EditText) findViewById(R.id.editText1)).getText().toString();
				String password = ((EditText) findViewById(R.id.editText2)).getText().toString();
				new HTTPTask(this).execute(addUserURL, user, password);
				disAbleButtons();
    		}
    	} catch (Exception e) {
    	}
    }
    
    public void login(View view) {
    	try {
    		if (checkUserPassword()) {
	        	String user = ((EditText) findViewById(R.id.editText1)).getText().toString();
	    		String password = ((EditText) findViewById(R.id.editText2)).getText().toString();
	    		new HTTPTask(this).execute(loginURL, user, password);
	    		disAbleButtons();
    		}
        } catch (Exception e) {
        }
    }
    
    public void disAbleButtons() {
		((Button) findViewById(R.id.button1)).setEnabled(false);
		((Button) findViewById(R.id.button1)).setTextColor(Color.rgb(255,255,255));
		((Button)findViewById(R.id.button2)).setEnabled(false);
		((Button) findViewById(R.id.button2)).setTextColor(Color.rgb(255,255,255));
	}
	public void EnAbleButtons() {
		((Button) findViewById(R.id.button1)).setEnabled(true);
		((Button) findViewById(R.id.button1)).setTextColor(Color.rgb(0,0,0));
		((Button) findViewById(R.id.button2)).setEnabled(true);
		((Button) findViewById(R.id.button2)).setTextColor(Color.rgb(0,0,0));
	}
	
	@Override
	protected void onRestart () {
		super.onRestart();
		EnAbleButtons();
		clearMessage();
	}
	
	public void setMessage(String message) {
		((TextView) findViewById(R.id.textView3)).setText(message);
	}
	
	public void addMessage(String message) {
		((TextView) findViewById(R.id.textView3)).setText(((TextView) findViewById(R.id.textView3)).getText()+message);
	}
	
	public void clearMessage() {
		((TextView) findViewById(R.id.textView3)).setText("");
	}
	
	public void jumpToDisplay(String user, String count) {
		Intent intent = new Intent(this, DisplayResult.class);
    	intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    	intent.putExtra(MainActivity.EXTRA_COUNT,count);
    	intent.putExtra(MainActivity.EXTRA_USER,user);
    	this.startActivity(intent);
	}
}



class HTTPTask extends AsyncTask<String, Void, String[]> {
	 Context context;
	 MainActivity activity;
	 HTTPTask(Context context) {
		 this.activity = (MainActivity) context;
	    this.context = context.getApplicationContext();
	 }
	
	
	
    protected String[] doInBackground(String ...strings ) {
    	String[] result = new String[2];
		try {
	    	HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(strings[0]);
			List<BasicNameValuePair> nameValuePairs = new ArrayList<BasicNameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("user", strings[1]));
			nameValuePairs.add(new BasicNameValuePair("password", strings[2]));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));
			StringBuilder builder = new StringBuilder();
			for (String line = null; (line = reader.readLine()) != null;) {
			    builder.append(line).append("\n");
			}
			JSONTokener tokener = new JSONTokener(builder.toString());
			JSONObject finalResult = new JSONObject(tokener);
			if (finalResult.getString("errCode").equals("1")) {
				result[0] = finalResult.getString("count");
				result[1] = strings[1];
				return result;
			} else {
				result[0] = finalResult.getString("errCode");
				return result;
			}
		} catch (Exception e) {
			result[0] = "-9999999";
			return result;
		}
    }

    protected void onProgressUpdate(Integer... progress) {
    }

    protected void onPostExecute(String ...result) {
    	int code = Integer.parseInt(result[0]);
    	if (code >=1) {
	    	
	    	//Cursor query = new SessionStorage(context).getReadableDatabase().query(SessionStorage.TABLE_NAME, null, null, null, null, null, null);
	    	//Cursor query = new SessionStorage(context).getWritableDatabase().query(SessionStorage.TABLE_NAME, null, null, null, null, null, null);
	    	//new SessionStorage(context).getWritableDatabase().insert(SessionStorage.TABLE_NAME, SessionStorage.count, result[0]);
	    	new SessionStorage(activity).addUserCount(result[1], result[0]);
	        activity.jumpToDisplay(result[1],result[0]);
    	} else {
    		((MainActivity) this.activity).EnAbleButtons();
    		switch (code) {
	    		case(-1):
	    			activity.setMessage("Bad Credentials");
	    			break;
    			case (-2):
    				activity.setMessage("User Exists");
    				break;
    			case (-3):
    				activity.setMessage("Bad Username");
    				break;
    			case (-4):
    				activity.setMessage("Bad Password");
    				break;
    			default:
    				activity.setMessage("Bad Connection");
    				break;
    		}
    	} 
    }
    
}









